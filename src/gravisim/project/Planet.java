package gravisim.project;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

/**
 * 
 * @author TheBestJB
 * Math class calculating the circular movement / orbit of a body.
 *
 */
public class Planet extends JComponent {

	private static final long serialVersionUID = 1L;
	private float vitesse = 0.01f;
	private float angle = 0;
	private int x, y, w, h, DiamX, DiamY;
	private BufferedImage img;
	private double distX, distY;
	
	/**
	 * Constructor receiving speed, distance and image name of the body.
	 * @param vitesse
	 * @param distX
	 * @param distY
	 * @param img
	 */
	public Planet(float vitesse, double distX, double distY, String img) {
		this.vitesse = vitesse;
		this.distX = distX;
		this.distY = distY;
		setDiamX((int) (distX*2));
		setDiamY((int) (distY*2));
		setToolTipText("tooltip");
		
		try {
			this.img = ImageIO.read(getClass().getResource(img));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Calculates the next position of the body according to the third Kepler's law.
	 * @param w
	 * @param h
	 */
	public void movement(int w, int h) {
		angle += vitesse;
		x = (int) ((distX * Math.cos(angle)));
		y = (int) ((distY * Math.sin(angle)));
		x += w;
		y += h;
	}
	
	/**
	 * Returns the graphical representation of the body and its position.
	 * @param w
	 * @param h
	 * @param p
	 * @param g2
	 * @param c
	 * @return Grapgics2D
	 */
	public Graphics2D getOrbit(int w, int h, Planet p, Graphics2D g2, Color c) {
		int minusX = (int) p.distX;
		int minusY = (int) p.distY;
		g2.setColor(c);
		g2.drawOval(w-minusX, h-minusY, p.getDiamX(), p.getDiamY());
		return g2;
	}

	/*
	 * GETTERS AND SETTERS
	 */
	public int getW() {
		return w;
	}

	public void setW(int w) {
		this.w = w;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public BufferedImage getImg() {
		return img;
	}

	public int getDiamX() {
		return DiamX;
	}

	public void setDiamX(int diamX) {
		DiamX = diamX;
	}

	public int getDiamY() {
		return DiamY;
	}

	public void setDiamY(int diamY) {
		DiamY = diamY;
	}

	public void setImg(BufferedImage img) {
		this.img = img;
	}
}
