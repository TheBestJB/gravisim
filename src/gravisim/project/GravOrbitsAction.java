package gravisim.project;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

/**
 * 
 * @author TheBestJB
 * AbstracAction who allows to verify is the orbits are displayed.
 *
 */
public class GravOrbitsAction extends AbstractAction {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor calling the verifState() method.
	 */
	public GravOrbitsAction() {
		// TODO Auto-generated constructor stub
		verifState();
	}
	
	/**
	 * Check if orbits are displayed and turns to true or false the Action (JCheckBox).
	 */
	private void verifState() {
		if (Pan1.isActiveOrbits()) {
			putValue(Action.SELECTED_KEY, true);
		}
		else {
			putValue(Action.SELECTED_KEY, false);
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		boolean isActive = (Pan1.isActiveOrbits()) ? false : true;
		Pan1.setActiveOrbits(isActive);
		verifState();
	}
}
