package gravisim.project;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * 
 * @author TheBestJB
 * JPanel of the main JFrame.
 *
 */
public class Pan1 extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;
	private static Timer t;
	private int h, w;
	private Planet sun, mercury, venus, earth, moon, mars, jupiter, saturn, uranus, neptune;
	private static boolean activeOrbits = false, isStopped = false;
	
	/**
	 * Sets the properties and start a timer to call regularly
	 * the paintComponent() method, drawing the bodies.
	 */
	public Pan1() {
		setLayout(new FlowLayout());
		setBackground(Color.black);
		setDoubleBuffered(true);
		initObjects();
		t = new Timer(10, this);
		t.start();
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		h = getSize().height/2;
		w = getSize().width/2;
		
		g2.drawImage(sun.getImg(), w-50, h-50, null);
		g2.drawImage(mercury.getImg(), mercury.getX(), mercury.getY(), null);
		g2.drawImage(venus.getImg(), venus.getX(), venus.getY(), null);
		g2.drawImage(earth.getImg(), earth.getX(), earth.getY(), null);
		g2.drawImage(moon.getImg(), moon.getX(), moon.getY(), null);
		g2.drawImage(mars.getImg(), mars.getX(), mars.getY(), null);
		g2.drawImage(jupiter.getImg(), jupiter.getX(), jupiter.getY(), null);
		g2.drawImage(saturn.getImg(), saturn.getX(), saturn.getY(), null);
		g2.drawImage(uranus.getImg(), uranus.getX(), uranus.getY(), null);
		g2.drawImage(neptune.getImg(), neptune.getX(), neptune.getY(), null);
		
		if (activeOrbits) {
			mercury.getOrbit(w, h, mercury, g2, new Color(145, 145, 145, 60));
			venus.getOrbit(w, h, venus, g2, new Color(204, 61, 0, 60));
			earth.getOrbit(w, h, earth, g2, new Color(0, 166, 255, 60));
			moon.getOrbit(earth.getX()+4, earth.getY()+4, moon, g2, new Color(255, 255, 255, 60));
			mars.getOrbit(w, h, mars, g2, new Color(255, 0, 0, 60));
			jupiter.getOrbit(w, h, jupiter, g2, new Color(255, 205, 173, 60));
			saturn.getOrbit(w, h, saturn, g2, new Color(255, 236, 173, 60));
			uranus.getOrbit(w, h, uranus, g2, new Color(173, 222, 255, 60));
			neptune.getOrbit(w, h, neptune, g2, new Color(0, 107, 179, 60));
		}
		
	}
	
	/**
	 * Initializes the bodies with their orbital speed, distance to the Sun and image.
	 */
	public void initObjects() {
		sun = new Planet(0, 0.0, 0.0, Bodies.SUN.img());
		mercury = new Planet(0.0415f, 70.0, 70.0, Bodies.MERCURY.img());
		venus = new Planet(0.01624f, 100.0, 100.0, Bodies.VENUS.img());
		earth = new Planet(0.01f, 150.0, 150.0, Bodies.EARTH.img());
		moon = new Planet(0.10f, 15.0, 15.0, Bodies.MOON.img());
		mars = new Planet(0.005322f, 202.0, 202.0, Bodies.MARS.img());
		jupiter = new Planet(0.000842f, 300.0, 300.0, Bodies.JUPITER.img());
		saturn = new Planet(0.000339f, 370.0, 370.0, Bodies.SATURN.img());
		uranus = new Planet(0.0001189f, 440.0, 440.0, Bodies.URANUS.img());
		neptune = new Planet(0.0000606f, 510.0, 510.0, Bodies.NEPTUNE.img());
	}

	/**
	 * Check if orbits are displayed.
	 * @return boolean
	 */
	public static boolean isActiveOrbits() {
		return activeOrbits;
	}

	/**
	 * Gets a boolean and sets the static one.
	 * @param activeOrbits
	 */
	public static void setActiveOrbits(boolean activeOrbits) {
		Pan1.activeOrbits = activeOrbits;
	}
	
	/**
	 * Stops or continue the orbital runs.
	 */
	public static void stopper() {
		if (isStopped) {
			isStopped = false;
			t.start();
		}
		else {
			isStopped = true;
			t.stop();
		}
	}

	/**
	 * Calls the method who calculates the next position of a body.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		mercury.movement(w, h);
		venus.movement(w, h);
		earth.movement(w, h);
		moon.movement(earth.getX()+2, earth.getY()+2);
		mars.movement(w, h);
		jupiter.movement(w, h);
		saturn.movement(w, h);
		uranus.movement(w, h);
		neptune.movement(w, h);
		repaint();
	}
}
