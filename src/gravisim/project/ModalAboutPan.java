package gravisim.project;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

/**
 * 
 * @author TheBestJB
 * Called by ModalAboutFrame and display the "À propos" text.
 *
 */
public class ModalAboutPan extends JPanel implements HyperlinkListener {

	private static final long serialVersionUID = 1L;
	private String projectLink = "https://bitbucket.org/TheBestJB/gravisim";
	
	/**
	 * Sets the properties and add texts and Hyperlink listener to the JEditorPane
	 */
	public ModalAboutPan() {
		setLayout(new BorderLayout());
		JEditorPane edPan = new JEditorPane("text/html", null);
		edPan.setEditable(false);
		edPan.setBackground(Color.white);
		String str1 = "<p style=\"text-align:center;\">GraviSim version : "+LoadingFrame.currentVer+"<br>";
		String str2 = "by TheBestJB<br><br>";
		String str3 = "<a href=\""+projectLink+"\">"+projectLink+"</a></p>";
		edPan.setText(str1+str2+str3);
		edPan.addHyperlinkListener(this);
		add(edPan, BorderLayout.CENTER);
		
	}

	@Override
	public void hyperlinkUpdate(HyperlinkEvent e) {
		HyperlinkEvent.EventType type = e.getEventType();
		if (type == HyperlinkEvent.EventType.ACTIVATED) {
			try {
			Desktop.getDesktop().browse(e.getURL().toURI());
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		}
		}
	}
}
