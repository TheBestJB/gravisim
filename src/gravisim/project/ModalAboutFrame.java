package gravisim.project;
import javax.swing.JDialog;

/**
 * 
 * @author TheBestJB
 * Sets the JDialog properties of the "À propos" window.
 *
 */
public class ModalAboutFrame extends JDialog {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Sets the properties and the JPanel (ModalAboutPan).
	 */
	public ModalAboutFrame() {
		setSize(300, 200);
		setTitle("À propos");
		setModal(true);
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setLocation(((GraviFrame.frame.getWidth()/2)-100), ((GraviFrame.frame.getHeight()/2)-50));
		setContentPane(new ModalAboutPan());
	}

}
