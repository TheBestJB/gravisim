package gravisim.project;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;

/**
 * 
 * @author TheBestJB
 * AbstractAction who gives an ArrayList<JMenuItem> containing all the bodies.
 * Each JMenuItem has an Action who calls the class ModalBoDyFrame (to pop up a JDialog).
 *
 */
public class BodiesInfoAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<String> bodiesList;
	private ModalBodyFrame modal;

	/**
	 * Constructor adding each bodies to the ArrayList.
	 * @param name
	 */
	public BodiesInfoAction(String name) {
		super(name);
		bodiesList = new ArrayList<String>();
		bodiesList.add(Bodies.SUN.toString());
		bodiesList.add(Bodies.MERCURY.toString());
		bodiesList.add(Bodies.VENUS.toString());
		bodiesList.add(Bodies.EARTH.toString());
		bodiesList.add(Bodies.MOON.toString());
		bodiesList.add(Bodies.MARS.toString());
		bodiesList.add(Bodies.JUPITER.toString());
		bodiesList.add(Bodies.SATURN.toString());
		bodiesList.add(Bodies.URANUS.toString());
		bodiesList.add(Bodies.NEPTUNE.toString());
	}
	
	/**
	 * Returns an ArrayList containing JMenuItem for each bodies and the associated Action.
	 * @return ArrayList<JMenuItem>
	 */
	public ArrayList<JMenuItem> getBodiesList() {
		Iterator<String> it = bodiesList.iterator();
		ArrayList<JMenuItem> menuBodies = new ArrayList<JMenuItem>();
		while (it.hasNext()) {
			JMenuItem mn = new JMenuItem(Bodies.valueOf(it.next()).fr());
			mn.addActionListener(this);
			menuBodies.add(mn);
		}
		return menuBodies;
	}
	
	/**
	 * Calls ModalBodyFrame class to pop up a JDialog showing body's info.
	 * @param name
	 */
	private void setDial(String name) {
		modal = new ModalBodyFrame(name);
		modal.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		setDial(e.getActionCommand());
		
	}

}
