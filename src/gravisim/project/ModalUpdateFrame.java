package gravisim.project;
import java.awt.Desktop;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 * 
 * @author TheBestJB
 * When called, check whether the version is the latest or not.
 * Then open a JOptionPane.
 *
 */
public class ModalUpdateFrame {

	private static JOptionPane pane;
	private static JDialog dial;
	
	/**
	 * Constructor calling the method who checks the version.
	 */
	public ModalUpdateFrame() {
		setDial();
	}
	
	/**
	 * Check the version.
	 * If it's up to date, it just show a simple JOptionPane message.
	 * Else it asks if the user want or don't want to download the latest version.
	 */
	private void setDial() {
		if (GraviFrame.isUpToDate)
		{
			JOptionPane.showMessageDialog(GraviFrame.frame, "Version à jour ! ( "+LoadingFrame.currentVer+" )",
					"Mise à jour", JOptionPane.INFORMATION_MESSAGE);
		}
		else {
			askForDownload();
		}
	}
	
	/**
	 * Open the JOptionPane asking for download.
	 */
	public static void askForDownload() {
		pane = new JOptionPane();
		dial = pane.createDialog("Mise à jour");
		dial.setSize(480, 110);
		pane.setMessageType(JOptionPane.QUESTION_MESSAGE);
		pane.setMessage("Une nouvelle version est disponible. Souhaitez-vous la télécharger ?");
		pane.setOptionType(0);
		dial.setVisible(true);
		
		Object dl = 0;
		if (pane.getValue() == dl) {
			URL dlUrl;
			try {
				dlUrl = new URL("http://www.c4k-team.com/GraviSim/GraviSim.jar");
				Desktop.getDesktop().browse(dlUrl.toURI());
			} catch (MalformedURLException e) {
				JOptionPane.showMessageDialog(GraviFrame.frame, "Une erreur s'est produite !",
						"Erreur", JOptionPane.WARNING_MESSAGE);
				e.printStackTrace();
			} catch (IOException e) {
				JOptionPane.showMessageDialog(GraviFrame.frame, "Une erreur s'est produite !",
						"Erreur", JOptionPane.WARNING_MESSAGE);
			} catch (URISyntaxException e) {
				JOptionPane.showMessageDialog(GraviFrame.frame, "Une erreur s'est produite !",
						"Erreur", JOptionPane.WARNING_MESSAGE);
			}
		}
	}
}
