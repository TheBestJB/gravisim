package gravisim.project;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * 
 * @author TheBestJB
 * JPanel loaded by LoadingFrame to draw the background image.
 *
 */
public class LoadingPan extends JPanel {

	private static final long serialVersionUID = 1L;
	private BufferedImage img;

	/**
	 * Constructor setting the layout and reading the background image.
	 */
	public LoadingPan() {
		setLayout(new FlowLayout());
		setBackground(Color.black);
		
		try {
			this.img = ImageIO.read(getClass().getResource("/img/loadImage.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(img, 0, 0, null);
	}
}
