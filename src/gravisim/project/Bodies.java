package gravisim.project;
/**
 * 
 * @author TheBestJB
 * Enumerates all the bodies of GraviSim.
 * Referencing all informations and URLs of each bodies.
 *
 */
public enum Bodies {

	SUN, MERCURY, VENUS, EARTH, MOON, MARS, JUPITER, SATURN, URANUS, NEPTUNE;
	
	/**
	 * Returns the path to the mini-png of the body.
	 * @return String
	 */
	public String img() {
		String str = " ";
		Bodies b = this;
		switch (b) {
		case SUN:
			str = "/img/Sun.png";
			break;
		case MERCURY:
			str = "/img/Mercury.png";
			break;
		case VENUS:
			str = "/img/Venus.png";
			break;
		case EARTH:
			str = "/img/Earth.png";
			break;
		case MOON:
			str = "/img/Moon.png";
			break;
		case MARS:
			str = "/img/Mars.png";
			break;
		case JUPITER:
			str = "/img/Jupiter.png";
			break;
		case SATURN:
			str = "/img/Saturn.png";
			break;
		case URANUS:
			str = "/img/Uranus.png";
			break;
		case NEPTUNE:
			str = "/img/Neptune.png";
			break;

		default:
			break;
		}
		return str;
	}
	
	/**
	 * Returns the path to the normal sized png of a body (modal's image).
	 * @return String
	 */
	public String img400() {
		String str = " ";
		Bodies b = this;
		switch (b) {
		case SUN:
			str = "/img/Sun400.png";
			break;
		case MERCURY:
			str = "/img/Mercury400.png";
			break;
		case VENUS:
			str = "/img/Venus400.png";
			break;
		case EARTH:
			str = "/img/Earth400.png";
			break;
		case MOON:
			str = "/img/Moon400.png";
			break;
		case MARS:
			str = "/img/Mars400.png";
			break;
		case JUPITER:
			str = "/img/Jupiter400.png";
			break;
		case SATURN:
			str = "/img/Saturn400.png";
			break;
		case URANUS:
			str = "/img/Uranus400.png";
			break;
		case NEPTUNE:
			str = "/img/Neptune400.png";
			break;

		default:
			break;
		}
		return str;
	}
	
	/**
	 * Returns the French name of a body.
	 * @return String
	 */
	public String fr() {
		String str = " ";
		Bodies b = this;
		switch (b) {
		case SUN:
			str = "Soleil";
			break;
		case MERCURY:
			str = "Mercure";
			break;
		case VENUS:
			str = "Vénus";
			break;
		case EARTH:
			str = "Terre";
			break;
		case MOON:
			str = "Lune";
			break;
		case MARS:
			str = "Mars";
			break;
		case JUPITER:
			str = "Jupiter";
			break;
		case SATURN:
			str = "Saturne";
			break;
		case URANUS:
			str = "Uranus";
			break;
		case NEPTUNE:
			str = "Neptune";
			break;

		default:
			break;
		}
		return str;
	}
	
	/**
	 * Takes the French version of a body and returns the associated Enum.
	 * @param str
	 * @return Bodies
	 */
	public static Bodies reverseFr(String str) {
		Bodies b = null;
		switch (str) {
		case "Soleil":
			b = SUN;
			break;
		case "Mercure":
			b = MERCURY;
			break;
		case "Vénus":
			b = VENUS;
			break;
		case "Terre":
			b = EARTH;
			break;
		case "Lune":
			b = MOON;
			break;
		case "Mars":
			b = MARS;
			break;
		case "Jupiter":
			b = JUPITER;
			break;
		case "Saturne":
			b = SATURN;
			break;
		case "Uranus":
			b = URANUS;
			break;
		case "Neptune":
			b = NEPTUNE;
			break;

		default:
			break;
		}
		return b;
	}
	
	/**
	 * Returns the html file's path containing informations about a body.
	 * @return String
	 */
	public String getInfos() {
		String str = " ";
		Bodies b = this;
		switch (b) {
		case SUN:
			str = "html/Sun.html";
			break;
		case MERCURY:
			str = "html/Mercury.html";
			break;
		case VENUS:
			str = "html/Venus.html";
			break;
		case EARTH:
			str = "html/Earth.html";
			break;
		case MOON:
			str = "html/Moon.html";
			break;
		case MARS:
			str = "html/Mars.html";
			break;
		case JUPITER:
			str = "html/Jupiter.html";
			break;
		case SATURN:
			str = "html/Saturn.html";
			break;
		case URANUS:
			str = "html/Uranus.html";
			break;
		case NEPTUNE:
			str = "html/Neptune.html";
			break;

		default:
			break;
		}
		return str;
	}
}
