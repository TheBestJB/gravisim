package gravisim.project;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

/**
 * 
 * @author TheBestJB
 * Creates a JMenuBar to implement in the (main) JFrame.
 * 
 */
public class GravMenu implements ActionListener {

	private JMenuBar bar;
	private JMenu menuOptions, menuBodies, menuHelp;
	private JCheckBoxMenuItem orbits;
	private JMenuItem stop, about, verifUpdt;
	
	/**
	 * Constructor who calls the setBar() method to initialize the JMenuBar.
	 */
	public GravMenu() {
		setBar();
	}

	/**
	 * @return bar
	 */
	public JMenuBar getBar() {
		return bar;
	}

	/**
	 * Called by constructor to set the menus.
	 */
	private void setBar() {
		bar = new JMenuBar();
		menuOptions = new JMenu("Options");
		menuOptions.setMnemonic('O');
		menuBodies = new JMenu("Corps");
		menuBodies.setMnemonic('C');
		menuHelp = new JMenu("?");
		
		orbits = new JCheckBoxMenuItem(new GravOrbitsAction());
		orbits.setText("Orbites");
		orbits.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.SHIFT_DOWN_MASK));
		orbits.setToolTipText("Affiche/Désaffiche les trajectoires orbitales");
		
		stop = new JMenuItem("Stopper / Reprendre");
		stop.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.SHIFT_DOWN_MASK));
		stop.addActionListener(this);
		
		verifUpdt = new JMenuItem("Vérifier mises à jour");
		verifUpdt.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, InputEvent.SHIFT_DOWN_MASK));
		verifUpdt.addActionListener(this);
		
		about = new JMenuItem("À propos");
		about.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0));
		about.addActionListener(this);
		
		BodiesInfoAction bia = new BodiesInfoAction("test");
		ArrayList<JMenuItem> listBodies = bia.getBodiesList();
		
		Iterator<JMenuItem> it = listBodies.iterator();
		while (it.hasNext()) {
			menuBodies.add(it.next());
		}
		
		menuOptions.add(orbits);
		menuOptions.addSeparator();
		menuOptions.add(stop);
		
		menuHelp.add(verifUpdt);
		menuHelp.add(about);
		
		bar.add(menuOptions);
		bar.add(menuBodies);
		bar.add(menuHelp);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Object source = e.getSource();
		if (source == stop) {
			Pan1.stopper();
		}
		if (source == verifUpdt) {
			new ModalUpdateFrame();
		}
		if (source == about) {
			ModalAboutFrame aboMod = new ModalAboutFrame();
			aboMod.setVisible(true);
		}
	}
}
