package gravisim.project;
import java.awt.Dimension;
import javax.swing.JFrame;

/**
 * 
 * @author TheBestJB
 * Main JFrame who comes after the launcher. It sets the window and get the JMenuBar.
 *
 */
public class GraviFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Pan1 container;
	private GravMenu bar;
	public static JFrame frame;
	public static boolean isUpToDate;
	
	/**
	 * Constructor who receives the boolean is-up-to-date (to check later if it is the latest version of GraviSim).
	 * Sets the JFrame.
	 * Call a JOptionPane is there is a new version available and asks for download.
	 * @param isUpToDate
	 */
	public GraviFrame(boolean isUpToDate) {
		setTitle("GraviSim");
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setSize(new Dimension(800, 600));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		GraviFrame.isUpToDate = isUpToDate;
		
		bar = new GravMenu();
		setJMenuBar(bar.getBar());
		
		container = new Pan1();
		setContentPane(container);
		
		frame = this;
		
		if (!isUpToDate) {
			ModalUpdateFrame.askForDownload();
		}
	}
}
