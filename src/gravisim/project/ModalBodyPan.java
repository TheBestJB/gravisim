package gravisim.project;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.JEditorPane;
import javax.swing.JPanel;


/**
 * 
 * @author TheBestJB
 * JPanel called by ModalBodyFrame.
 * Receiving a body name and displaying its informations.
 *
 */
public class ModalBodyPan extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BufferedImage img;
	private Bodies b;
	private URL resHtml;
	
	/**
	 * Sets the properties and instantiates a JEditorPane 
	 * who reads the body's html file containing the informations.
	 * @param b
	 */
	public ModalBodyPan(Bodies b) {
		setLayout(new FlowLayout(FlowLayout.LEFT, 10, 400));
		JEditorPane edPan = new JEditorPane();
		setBackground(Color.white);
		this.b = b;
		setImg(this.b);
		
		resHtml = getClass().getResource("/"+b.getInfos());
		try {
			edPan.setContentType("text/html; charset=UTF-8");
			edPan.setPage(resHtml);
			edPan.setEditable(false);
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		add(edPan);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.drawImage(img, 0, 0, null);
	}
	
	/**
	 * Receives an Enum body and gets the associated image's path.
	 * @param b
	 */
	private void setImg(Bodies b) {
		try {
			img = ImageIO.read(getClass().getResource(b.img400()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}