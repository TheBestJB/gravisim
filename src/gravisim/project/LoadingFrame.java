package gravisim.project;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.swing.JFrame;
import javax.swing.Timer;

/**
 * 
 * @author TheBestJB
 * Launcher of 10 seconds.
 * Checks is the running version is the latest version by connecting to a server and parsing a .txt.
 *
 */
public class LoadingFrame extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private Timer t;
	public static final double currentVer = 1.2;
	private boolean isUpToDate;
	
	/**
	 * Constructor setting the JFrame properties and check the GraviSim's version.
	 * Then launch the main JFrame and gives it a boolean (true = up to date).
	 */
	public LoadingFrame() {
		Toolkit tk = Toolkit.getDefaultToolkit();
		Dimension screen = tk.getScreenSize();
		setLocation(((screen.width/2)-250), ((screen.height/2)-250));
		setSize(500, 500);
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		LoadingPan pan = new LoadingPan();
		add(pan);
		t = new Timer(10000, this);
		t.start();
		
		try {
			URL vUrl = new URL("http://www.c4k-team.com/GraviSim/version.txt");
			URLConnection vConnec = vUrl.openConnection();
			BufferedReader input = new BufferedReader(new InputStreamReader(vConnec.getInputStream()));
			String version = input.readLine();
			if (version != null) {
				double lastVersion = Double.parseDouble(version);
				if (lastVersion != currentVer) {
					isUpToDate = false;
				}
				else {
					isUpToDate = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		t.stop();
		dispose();
		GraviFrame fen = new GraviFrame(isUpToDate);
		fen.setVisible(true);
	}

}
