package gravisim.project;
import javax.swing.JDialog;

/**
 * 
 * @author TheBestJB
 * Sets a JDialog and sets a JPanel to display the body's informations.
 *
 */
public class ModalBodyFrame extends JDialog {

	private static final long serialVersionUID = 1L;
	private ModalBodyPan pan;
	
	/**
	 * Constructor taking a body name and giving the associated Enum name to the JPanel.
	 * @param name
	 */
	public ModalBodyFrame(String name) {
		super(GraviFrame.frame, "Fiche "+name, true);
		setSize(415, 700);
		setLocation(100, 100);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setResizable(false);
		pan = new ModalBodyPan(Bodies.reverseFr(name));
		setContentPane(pan);
	}

}
